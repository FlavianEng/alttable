import { Router } from 'express';
import produtRouter from './app/routes/product.routes';
import seatingPlanRouter from './app/routes/seatingPlan.routes';
import serviceRouter from './app/routes/service.routes';
import cardRouter from './app/routes/card.routes';
import tableRouter from './app/routes/table.routes';
import clientRouter from './app/routes/client.routes';

const router = Router();

router.use('/product', produtRouter);
router.use('/card', cardRouter);
router.use('/seatingPlan', seatingPlanRouter);
router.use('/service', serviceRouter);
router.use('/table', tableRouter);
router.use('/client', clientRouter);

export default router;
