import * as express from 'express';
import router from './router';

// Create express application
export const app = express();

// Parsed in json
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Routes
app.use('/api', router);
