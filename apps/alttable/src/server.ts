import { app } from './app';
import * as mongoose from 'mongoose';
import * as dotenv from 'dotenv';

// Environment variable
dotenv.config({ path: process.cwd() + '/.env' });

// Port
const port = process.env.PORT || 3333;

// Start the application
app
  .listen(port, () => {
    mongoose.connect(`${process.env.MONGO_URI}`).then(() => {
      console.log('The database is up ! 😎');
    });
    console.log(`⚡️ Listening at http://localhost:${port}/api ⚡️`);
  })
  .on('error', console.error);
