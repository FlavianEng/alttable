import * as dotenv from 'dotenv';
import * as request from 'supertest';
import { app } from '../app';
import HttpStatusCode from '../app/helpers/httpStatusCode.helper';
import {
  connectToTestDatabase,
  dropTestDatabaseCollection,
} from './utils/db.utils';
import { createSeatingPlan } from './utils/seating.utils';
import {
  createService,
  exampleBadServiceToCreate1,
  exampleBadServiceToCreate2,
  exampleBadServiceToCreate3,
  exampleGoodServiceToCreate,
} from './utils/service.utils';

// Get the environment variable
dotenv.config({ path: process.cwd() + '/.env' });

// Test
describe('/service endpoint', () => {
  beforeAll(async () => {
    await connectToTestDatabase();
  });

  afterEach(async () => {
    await dropTestDatabaseCollection('seatingplans');
    await dropTestDatabaseCollection('services');
  });

  describe('[POST] When creating a service', () => {
    it('Should create a service', async () => {
      await createSeatingPlan();
      const {
        body: { data, status, statusCode },
      } = await createService();

      expect(data.id).toBeDefined();
      expect(status).toEqual('success');
      expect(statusCode).toEqual(HttpStatusCode.CREATED);
    });

    it('Should have an error of closing date not greater than opening date', async () => {
      const {
        body: { message, status, statusCode },
      } = await request(app)
        .post('/api/service')
        .send(exampleBadServiceToCreate1)
        .expect(HttpStatusCode.BAD_REQUEST);

      expect(message).toEqual('Closing date cannot be lower than opening date');
      expect(status).toEqual('error');
      expect(statusCode).toEqual(HttpStatusCode.BAD_REQUEST);
    });

    it('Should have an error of parameter undefined', async () => {
      const {
        body: { message, status, statusCode },
      } = await request(app)
        .post('/api/service')
        .send(exampleBadServiceToCreate2)
        .expect(HttpStatusCode.BAD_REQUEST);

      expect(message).toEqual('Closing date is undefined');
      expect(status).toEqual('error');
      expect(statusCode).toEqual(HttpStatusCode.BAD_REQUEST);
    });

    it('Should have an error if not plan table exists', async () => {
      const {
        body: { message, status, statusCode },
      } = await request(app)
        .post('/api/service')
        .send(exampleGoodServiceToCreate)
        .expect(HttpStatusCode.BAD_REQUEST);

      expect(message).toEqual(
        'To create a service, a table plan must already be created'
      );
      expect(status).toEqual('error');
      expect(statusCode).toEqual(HttpStatusCode.BAD_REQUEST);
    });

    it('Should have an error of date not valid', async () => {
      const {
        body: { message, status, statusCode },
      } = await request(app)
        .post('/api/service')
        .send(exampleBadServiceToCreate3)
        .expect(HttpStatusCode.BAD_REQUEST);

      expect(message).toEqual('Opening date is not a valid date');
      expect(status).toEqual('error');
      expect(statusCode).toEqual(HttpStatusCode.BAD_REQUEST);
    });
  });
});
