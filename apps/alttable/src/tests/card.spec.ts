import { app } from '../app';
import * as request from 'supertest';
import * as dotenv from 'dotenv';
import { connectToTestDatabase, dropTestDatabaseCollection } from './utils/db.utils';

// Get the environment variable
dotenv.config({ path: process.cwd() + '/.env' });

// Example data
const exempleGoodProduct = {
  name: 'Oeuf au plat façon Kenan',
  description: 'Un oeuf avec un peu de persil',
  type: 'Plat',
  price: 99,
  quantity: 5,
};

// Test
describe('/card endpoint', () => {
  beforeAll(async () => {
    await connectToTestDatabase();
  });

  afterAll(async () => {
    await dropTestDatabaseCollection('products');
  });

  describe('[GET] card', () => {
    it('[GET] Should send an array of product where quantity > 1', async () => {

      await request(app).post('/api/product/add').send(exempleGoodProduct).expect(201);
      await request(app).post('/api/product/add').send({...exempleGoodProduct, name: 'test', quantity: 0}).expect(201);

      const cardList = await request(app).get('/api/card/').send().expect(200)
      expect(cardList.body.data.length).toBeGreaterThanOrEqual(1)
    });
  });
});
