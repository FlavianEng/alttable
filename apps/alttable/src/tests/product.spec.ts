import { app } from '../app';
import * as request from 'supertest';
import * as dotenv from 'dotenv';
import {
  connectToTestDatabase,
  dropTestDatabaseCollection,
} from './utils/db.utils';

// Get the environment variable
dotenv.config({ path: process.cwd() + '/.env' });

// Example data
const exempleGoodProduct = {
  name: 'Oeuf au plat façon Kenan',
  description: 'Un oeuf avec un peu de persil',
  type: 'Plat',
  price: 99,
  quantity: 5,
};

// Test
describe('/product endpoint', () => {
  beforeAll(async () => {
    await connectToTestDatabase();
  });

  afterAll(async () => {
    await dropTestDatabaseCollection('products');
  });

  describe('[POST] When creating an order', () => {
    it('[POST] Should create a product', async () => {
      const { body } = await request(app).post('/api/product/add').send(exempleGoodProduct).expect(201);
      expect(body.data.id).toBeDefined();
    });

    it('[POST] Should return an error if product already exist', async () => {
      const { body } = await request(app).post('/api/product/add').send(exempleGoodProduct).expect(400);
      expect(body.message).toBeDefined();
    });

    it('[POST] Should return an error if product type is undefined or not correct enum', async () => {
      const errorProduct = {...exempleGoodProduct, name: 'test', type: 'qsd'}
      const { body } = await request(app).post('/api/product/add').send(errorProduct).expect(400);
      expect(body.message).toBeDefined();
    });
  });

  describe('[PUT] When update a product', () => {
    it('[PUT] Should update a product', async () => {
      const updatedProduct = await request(app).put('/api/product/update/quantity').send({ name: exempleGoodProduct.name, quantity: 9 }).expect(200);
      expect(updatedProduct.body.data.quantity).toBe(9);
    });

    it('[PUT] Should return an error when update a product who doesnt exist', async () => {
      const updatedProduct = await request(app).put('/api/product/update/quantity').send({ name: "JEXISTEPASOLOL" }, { quantity: 9 }).expect(404);
      expect(updatedProduct.body.message).toBeDefined();
    });
  });

  describe('[GET] All products', () => {
    it('[GET] Should send an array of product', async () => {
      const productList = await request(app).get('/api/product/').send().expect(200)
      expect(productList.body.data).toBeDefined()
    });
  });
});
