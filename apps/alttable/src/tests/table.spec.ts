import { app } from '../app';
import * as request from 'supertest';
import * as dotenv from 'dotenv';
import {
  connectToTestDatabase,
  dropTestDatabaseCollection,
} from './utils/db.utils';
import {
  exampleTable,
  createTable,
  generateRandomTable,
  getAllTables,
  openTable,
} from './utils/table.utils';

// Get the environment variable
dotenv.config({ path: process.cwd() + '/.env' });

describe('/table endpoint', () => {
  beforeAll(async () => {
    await connectToTestDatabase();
  });

  afterAll(async () => {
    await dropTestDatabaseCollection('tables');
  });

  it('[POST] Should create a table', async () => {
    const {
      body: { data },
    } = await createTable(exampleTable, 201);

    expect(data.id).toBeDefined();
  });

  it('[POST] Should throw an error when a table number already exists', async () => {
    const randomTable = generateRandomTable();
    randomTable.tableNumber = 0;

    await createTable(randomTable, 201);
    const {
      body: { message },
    } = await createTable(randomTable, 400);

    expect(message).toEqual(
      '🚨 Validator failed : The field  table number is not unique'
    );
  });

  it('[POST] Should throw an error when a number of seats is not strictly positive', async () => {
    const randomTable = generateRandomTable();
    randomTable.numberOfSeats = 0;

    const {
      body: { message },
    } = await createTable(randomTable, 400);

    expect(message).toEqual(
      '🚨 Validator failed : The field  Number of seats is not strictly positive'
    );
  });

  it('[GET] Should get a table by table number', async () => {
    const randomTable = generateRandomTable();
    const { tableNumber } = randomTable;

    await createTable(randomTable, 201);

    const {
      body: { data },
    } = await request(app).get(`/api/table/number/${tableNumber}`).expect(200);

    expect(data.id).toBeDefined();
  });

  it('[GET] Should get all tables', async () => {
    const {
      body: { data },
    } = await getAllTables();

    expect(data.length > 0).toBeTruthy();
  });

  it('[PUT] Should open table', async () => {
    const randomTable = generateRandomTable();
    await createTable(randomTable, 201);
    const { numberOfSeats, tableNumber } = randomTable;
    const numberOfClients =
      numberOfSeats > 2 ? numberOfSeats - 1 : numberOfSeats;

    const {
      body: { data },
    } = await openTable(tableNumber, numberOfClients, 200);

    expect(data.clients.length).toEqual(numberOfClients);
    expect(data.tableState).toEqual('started');
    expect(data.available).toEqual(false);
  });

  it('[PUT] Should fail to open table because tableNumber is not defined', async () => {
    const randomTable = generateRandomTable();
    await createTable(randomTable, 201);
    const { numberOfSeats } = randomTable;
    const numberOfClients =
      numberOfSeats > 2 ? numberOfSeats - 1 : numberOfSeats;

    const {
      body: { message },
    } = await openTable(1000000, numberOfClients, 400);

    expect(message).toEqual('This number of table is undefined');
  });

  it('[PUT] Should fail to open table because number of clients is not positive', async () => {
    const randomTable = generateRandomTable();
    await createTable(randomTable, 201);
    const { tableNumber } = randomTable;

    const {
      body: { message },
    } = await openTable(tableNumber, 0, 400);
    expect(message).toEqual(
      '🚨 Validator failed : The field  numberOfClients is not strictly positive'
    );
  });

  it('[PUT] Should fail to open table because table is not available', async () => {
    const randomTable = generateRandomTable();
    randomTable.available = false;
    await createTable(randomTable, 201);

    const { numberOfSeats, tableNumber } = randomTable;
    const numberOfClients =
      numberOfSeats > 2 ? numberOfSeats - 1 : numberOfSeats;

    const {
      body: { message },
    } = await openTable(tableNumber, numberOfClients, 400);
    expect(message).toEqual('Table availability is not set to true');
  });

  it('[PUT] Should fail to open table because number of clients is greater than the number of seats', async () => {
    const randomTable = generateRandomTable();
    await createTable(randomTable, 201);
    const { numberOfSeats, tableNumber } = randomTable;
    const numberOfClients = numberOfSeats + 2;

    const {
      body: { message },
    } = await openTable(tableNumber, numberOfClients, 400);
    expect(message).toEqual(
      'The number of client inputed cannot be greater than the number of seats'
    );
  });
});
