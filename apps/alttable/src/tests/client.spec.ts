import * as dotenv from 'dotenv';
import {
  connectToTestDatabase,
  dropTestDatabaseCollection,
} from './utils/db.utils';

import { exampleClient, createClient } from './utils/client.utils';

// Get the environment variable
dotenv.config({ path: process.cwd() + '/.env' });

describe('/client endpoint', () => {
  beforeAll(async () => {
    await connectToTestDatabase();
  });

  afterAll(async () => {
    await dropTestDatabaseCollection('clients');
  });

  it('Should create a new client', async () => {
    const {
      body: { data },
    } = await createClient(exampleClient, 201);

    expect(data.id).toBeDefined();
  });
});
