import * as request from 'supertest';
import { app } from '../../app';
import { ClientToCreateDto } from '../../app/dto/client.dto';

export const exampleClient = {
  tableId: '61a6a1bf11b9827fbfd1484a',
  tableSeatNumber: 1,
};

export const createClient = async (
  clientData: ClientToCreateDto,
  statusCodeExpected: number
) => {
  return await request(app)
    .post('/api/client')
    .send(clientData)
    .expect(statusCodeExpected);
};
