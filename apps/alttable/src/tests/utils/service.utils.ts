import * as request from 'supertest';
import HttpStatusCode from '../../app/helpers/httpStatusCode.helper';
import { app } from './../../app';

// Example good data
export const exampleGoodServiceToCreate = {
  openingDate: '2021-11-25 11:00:00',
  closingDate: '2021-11-25 14:00:00',
};

// Example bad data
export const exampleBadServiceToCreate1 = {
  openingDate: '2021-11-25 11:00:00',
  closingDate: '2021-11-25 10:00:00',
};

export const exampleBadServiceToCreate2 = {
  openingDate: '2021-11-25 11:00:00',
};

export const exampleBadServiceToCreate3 = {
  openingDate: 'Lorem ipsum',
  closingDate: '2021-11-25 10:00:00',
};

// Functions
export const createService = async () => {
  return request(app)
    .post('/api/service')
    .send(exampleGoodServiceToCreate)
    .expect(HttpStatusCode.CREATED);
};
