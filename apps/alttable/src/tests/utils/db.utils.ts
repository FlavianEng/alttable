import * as mongoose from 'mongoose';

export const connectToTestDatabase = async () => {
  await mongoose.connect(`${process.env.MONGO_URI_TEST}`);
};

export const dropTestDatabaseCollection = async (
  collectionNameToDrop: string
) => {
  const db = mongoose.connection.db;

  // Get all collections
  const collections = await db.listCollections().toArray();

  // Create an array of collection names and drop each collection
  collections
    .map((collection) => collection.name)
    .forEach(async (collectionName) => {
      if (collectionName === collectionNameToDrop) {
        db.dropCollection(collectionName);
      }
    });
};
