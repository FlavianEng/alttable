import * as request from 'supertest';
import { app } from '../../app';
import { TableToCreateDto } from '../../app/dto/table.dto';

export const exampleTable = {
  numberOfSeats: 4,
  available: true,
  tableNumber: 1,
  tableState: 'notStarted',
};

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function generateRandomTable() {
  return {
    numberOfSeats: getRandomIntInclusive(2, 10),
    available: true,
    tableNumber: getRandomIntInclusive(2, 10000),
    tableState: 'notStarted',
  };
}

function generateMultipleRandomTable(numberOfTable: number) {
  const tables = [];
  for (let i = 0; i < numberOfTable; i++) {
    const table = generateRandomTable();
    tables.push(table);
  }
  return tables.length === 1 ? tables[0] : tables;
}

export const createTable = async (
  table: TableToCreateDto,
  statusCodeExpected: number
) => {
  return await request(app)
    .post('/api/table')
    .send(table)
    .expect(statusCodeExpected);
};

export const getAllTables = async () => {
  return await request(app).get('/api/table').expect(200);
};

export const createMultipleTables = async (numberOfTable: number) => {
  const tables = generateMultipleRandomTable(numberOfTable);

  await Promise.all(
    tables.map(async (table) => {
      await createTable(table, 201);
    })
  );
};

export const openTable = async (
  tableNumber: number,
  numberOfClients: number,
  statusCodeExpected: number
) => {
  return await request(app)
    .put('/api/table')
    .query({ tableNumber, numberOfClients })
    .expect(statusCodeExpected);
};
