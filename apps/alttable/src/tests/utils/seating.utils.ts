import * as request from 'supertest';
import { app } from '../../app';
import { createMultipleTables, getAllTables } from '../utils/table.utils';

export const createSeatingPlan = async () => {
  await createMultipleTables(3);

  const {
    body: { data: tables },
  } = await getAllTables();
  const data = { tables };

  return await request(app).post('/api/seatingPlan').send(data).expect(201);
};

export const getSeatingPlan = async () => {
  return await request(app).get('/api/seatingPlan').expect(200);
};
