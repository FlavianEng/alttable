import { app } from '../app';
import * as request from 'supertest';
import * as dotenv from 'dotenv';
import {
  connectToTestDatabase,
  dropTestDatabaseCollection,
} from './utils/db.utils';
import { createSeatingPlan, getSeatingPlan } from './utils/seating.utils';

// Get the environment variable
dotenv.config({ path: process.cwd() + '/.env' });

describe('/seatingPlan endpoint', () => {
  beforeAll(async () => {
    await connectToTestDatabase();
  });

  afterAll(async () => {
    await dropTestDatabaseCollection('seatingplans');
  });

  it('[POST] Should fail because undefined', async () => {
    const {
      body: { message },
    } = await request(app).post('/api/seatingPlan').send({}).expect(400);

    expect(message).toEqual(
      '🚨 Validator failed : seatingPlanToCreate is undefined'
    );
  });

  it('[POST] Should fail because tables is empty', async () => {
    const {
      body: { message },
    } = await request(app)
      .post('/api/seatingPlan')
      .send({ tables: [] })
      .expect(400);

    expect(message).toEqual(
      '🚨 Validator failed : The field tables has a negative length'
    );
  });

  it('[POST] Should create a seating plan', async () => {
    const {
      body: { data },
    } = await createSeatingPlan();
    expect(data.id).toBeDefined();
  });

  it('[GET] Should get the seating plan', async () => {
    await createSeatingPlan();
    const {
      body: {
        data: { id, tables },
      },
    } = await getSeatingPlan();

    expect(id).toBeDefined();
    expect(tables[0]).toHaveProperty('tableNumber');
    expect(tables[0]).toHaveProperty('numberOfSeats');
  });
});
