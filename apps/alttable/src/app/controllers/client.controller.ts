import { ClientToCreateDto } from '../dto/client.dto';
import * as clientService from '../services/client.service';
import { sendSuccess, sendError } from '../helpers/handleResponse.helper';
import HttpStatusCode from '../helpers/httpStatusCode.helper';

export const create = async (req, res) => {
  const clientToCreate: ClientToCreateDto = req.body;

  try {
    const clientCreated = await clientService.create(clientToCreate);
    sendSuccess(clientCreated, HttpStatusCode.CREATED, res);
  } catch (error) {
    sendError(error, res);
  }
};
