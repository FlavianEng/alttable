import { SimplifiedTableToOpenDto, TableToCreateDto } from '../dto/table.dto';
import * as tableService from '../services/table.service';
import * as clientService from '../services/client.service';
import {
  isUniqueField,
  isNumberStrictlyPositive,
  isTrue,
  firstParamIsLowerOrEqual,
  isDefined,
} from '../helpers/validators.helper';
import { sendError, sendSuccess } from '../helpers/handleResponse.helper';
import HttpStatusCode from '../helpers/httpStatusCode.helper';

export const create = async (req, res) => {
  const tableToCreate: TableToCreateDto = req.body;

  try {
    const { numberOfSeats, tableNumber } = tableToCreate;
    await isUniqueField(
      tableService.getByTableNumber,
      tableNumber,
      'table number'
    );

    isNumberStrictlyPositive(numberOfSeats, 'Number of seats');

    const tableCreated = await tableService.create(tableToCreate);
    sendSuccess(tableCreated, HttpStatusCode.CREATED, res);
  } catch (error) {
    sendError(error, res);
  }
};

// TASK Send an error if table number does not exist
export const getByTableNumber = async (req, res) => {
  const tableNumber: number = req.params.tableNumber;

  try {
    const table = await tableService.getByTableNumber(tableNumber);
    sendSuccess(table, HttpStatusCode.OK, res);
  } catch (error) {
    sendError(error, res);
  }
};

export const getAll = async (req, res) => {
  try {
    const tables = await tableService.getAll();
    sendSuccess(tables, HttpStatusCode.OK, res);
  } catch (error) {
    sendError(error, res);
  }
};

export const openTable = async (req, res) => {
  try {
    const tableToOpen: SimplifiedTableToOpenDto = req.query;
    const { tableNumber, numberOfClients } = tableToOpen;

    const table = await tableService.getByTableNumber(tableNumber);

    isDefined(table, 'This number of table');
    isNumberStrictlyPositive(numberOfClients, 'numberOfClients');
    isTrue(table.available, 'Table availability');
    firstParamIsLowerOrEqual(
      numberOfClients,
      table.numberOfSeats,
      'The number of client inputed',
      'the number of seats'
    );

    // TASK A service must be active

    const clientPromises = [];

    for (let i = 0; i < numberOfClients; i++) {
      const body = { tableId: table.id, tableSeatNumber: i };
      const clientPromise = await Promise.resolve(clientService.create(body));
      clientPromises.push(clientPromise);
    }

    const clientsIds = await Promise.all(clientPromises);
    const clients = clientsIds.map((clientId) => clientId.id);

    const openTableData = {
      tableNumber,
      clients,
    };

    const openTable = await tableService.openTable(openTableData);
    sendSuccess(openTable, HttpStatusCode.OK, res);
  } catch (error) {
    sendError(error, res);
  }
};
