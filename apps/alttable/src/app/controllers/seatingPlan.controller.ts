import { SeatingPlanToCreateDto } from '../dto/seatingPlan.dto';
import * as seatingPlanService from '../services/seatingPlan.service';
import { hasPositiveLength, isObjectDefined } from '../helpers/validators.helper';
import { sendError, sendSuccess } from '../helpers/handleResponse.helper';
import HttpStatusCode from '../helpers/httpStatusCode.helper';

export const create = async (req, res) => {
  const seatingPlanToCreate: SeatingPlanToCreateDto = req.body;

  try {
    isObjectDefined(seatingPlanToCreate, 'seatingPlanToCreate');
    const { tables } = seatingPlanToCreate;
    hasPositiveLength(tables, 'tables');

    const seatingPlanCreated = await seatingPlanService.create(
      seatingPlanToCreate
    );
    sendSuccess(seatingPlanCreated, HttpStatusCode.CREATED, res);
  } catch (error) {
    sendError(error, res);
  }
};

export const get = async (req, res) => {
  try {
    const seatingPlan = await seatingPlanService.get();
    sendSuccess(seatingPlan, HttpStatusCode.OK, res);
  } catch (error) {
    sendError(error, res);
  }
};
