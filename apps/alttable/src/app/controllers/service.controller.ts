import HttpStatusCode from '../helpers/httpStatusCode.helper';
import * as serviceService from '../services/service.service';
import {
  isDate,
  isDefined,
  isGreaterOrEqual,
} from '../helpers/validators.helper';
import { ServiceDetailsDto, ServiceReceivedDto } from './../dto/service.dto';
import { sendError, sendSuccess } from './../helpers/handleResponse.helper';

// Create a service
export const create = async (req, res) => {
  const serviceToCreate: ServiceReceivedDto = req.body;

  // TODO : Permission granted if the role is "manager"

  try {
    // Valid data
    isDefined(serviceToCreate.openingDate, 'Opening date');
    isDefined(serviceToCreate.closingDate, 'Closing date');

    isDate(serviceToCreate.openingDate, 'Opening date');
    isDate(serviceToCreate.closingDate, 'Closing date');

    isGreaterOrEqual(
      serviceToCreate.closingDate,
      serviceToCreate.openingDate,
      'Closing date',
      'opening date'
    );

    const serviceDetails: ServiceDetailsDto = await serviceService.create(
      serviceToCreate
    );

    sendSuccess(serviceDetails, HttpStatusCode.CREATED, res);
  } catch (error) {
    sendError(error, res);
  }
};
