import { ProductToCreateDto, ProductCreatedDto, ProductToUpdateDto } from '../dto/product.dto';
import { sendError, sendSuccess } from '../helpers/handleResponse.helper';
import HttpStatusCode from '../helpers/httpStatusCode.helper';
import * as productService from '../services/product.service';

// Create
export const create = async (req, res) => {
 const productToCreate: ProductToCreateDto = req.body

  try {
    const productCreated = await productService.create(productToCreate)
    sendSuccess(productCreated, HttpStatusCode.CREATED, res)
  } catch (error) {
    sendError(error, res)
  }
}

export const updateQuantity = async (req, res) => {
  const productToUpdate: ProductToUpdateDto = req.body
   try {
     const productUpdated = await productService.updateQuantity(productToUpdate)
     sendSuccess(productUpdated, HttpStatusCode.OK, res)
   } catch (error) {
     sendError(error, res)
   }
 }

export const getAll = async (req, res) => {
   try {
     const productList = await productService.getAllProduct()
     sendSuccess(productList, HttpStatusCode.OK, res)
   } catch (error) {
     sendError(error, res)
   }
}