import { sendError, sendSuccess } from '../helpers/handleResponse.helper';
import HttpStatusCode from '../helpers/httpStatusCode.helper';
import * as cardService from '../services/card.service';

export const getCard = async (req, res) => {
  try {
    const menuList = await cardService.getCard()
    sendSuccess(menuList, HttpStatusCode.OK, res)
  } catch (error) {
    sendError(error, res)
  }
}