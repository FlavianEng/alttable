import { Schema } from 'mongoose';

const serviceSchema = new Schema({
  openingDate: {
    type: Date,
    required: [true, 'Opening date is required'],
  },
  closingDate: {
    type: Date,
    required: [true, 'Closing date is required'],
  },
  seatingPlanId: {
    type: Schema.Types.ObjectId,
    ref: 'SeatingPlan',
    required: [true, 'Seating plan is required'],
  },
});

serviceSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

export default serviceSchema;
