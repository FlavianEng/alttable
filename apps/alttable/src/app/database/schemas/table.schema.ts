import { Schema, Types } from 'mongoose';

const tableStateEnum = ['notStarted', 'started', 'finished'];

const tableSchema = new Schema({
  numberOfSeats: {
    type: Number,
    required: true,
  },
  clients: { type: [Types.ObjectId], ref: 'client', default: [] },
  available: { type: Boolean, default: true },
  // order: {type: orderId},
  tableNumber: { type: Number, required: true },
  // check: {type: checkId},
  tableState: { type: String, enum: tableStateEnum, default: 'notStarted' },
});

tableSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

export default tableSchema;
