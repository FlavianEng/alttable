import { Schema } from 'mongoose';

const productSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    enum: ['Apéritif', 'Entrée', 'Plat', 'Dessert', 'Boisson'],
  },
  quantity: {
    type: Number,
    default: 0,
  },
  price: {
    type: Number,
  },
});

productSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

export default productSchema;
