import { Schema, Types } from 'mongoose';

const seatingPlanSchema = new Schema(
  {
    tables: [{ type: Types.ObjectId, ref: 'table' }],
  },
  {
    timestamps: true,
  }
);

seatingPlanSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

export default seatingPlanSchema;
