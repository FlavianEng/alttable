import { Schema, Types } from 'mongoose';

const clientSchema = new Schema({
  tableId: {
    type: Types.ObjectId,
    ref: 'table',
    required: [true, 'The table id is required'],
  },
  tableSeatNumber: {
    type: Number,
    required: [true, 'The table seat number is required'],
  },
});

clientSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

export default clientSchema;
