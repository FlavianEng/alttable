import * as mongoose from 'mongoose';
import serviceSchema from '../schemas/service.schema';

const serviceModel = mongoose.model('service', serviceSchema);

export default serviceModel;
