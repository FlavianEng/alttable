import * as mongoose from 'mongoose';
import productSchema from '../schemas/product.schema';

const ProductModel = mongoose.model('product', productSchema);

export default ProductModel;
