import * as mongoose from 'mongoose';
import seatingPlanSchema from '../schemas/seatingPlan.schema';

const SeatingPlanModel = mongoose.model('seatingPlan', seatingPlanSchema);

export default SeatingPlanModel;
