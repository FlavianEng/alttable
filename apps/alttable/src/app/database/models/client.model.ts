import * as mongoose from 'mongoose';
import clientSchema from '../schemas/client.schema';

const clientModel = mongoose.model('client', clientSchema);

export default clientModel;
