import * as mongoose from 'mongoose';
import tableSchema from '../schemas/table.schema';

const TableModel = mongoose.model('table', tableSchema);

export default TableModel;
