import ServiceModel from '../database/models/service.model';
import { ErrorHandler } from '../helpers/handleResponse.helper';
import HttpStatusCode from '../helpers/httpStatusCode.helper';
import { ServiceToCreateDto, ServiceDetailsDto } from './../dto/service.dto';

// Create a service
export const create = async (
  serviceToCreate: ServiceToCreateDto
): Promise<ServiceDetailsDto> => {
  try {
    const serviceDetails: ServiceDetailsDto = await new ServiceModel(
      serviceToCreate
    ).save();
    return serviceDetails;
  } catch (error) {
    throw new ErrorHandler(error.message, HttpStatusCode.BAD_REQUEST);
  }
};
