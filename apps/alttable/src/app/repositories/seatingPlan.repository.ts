import {
  SeatingPlanCreatedDto,
  SeatingPlanToCreateDto,
  SeatingPlanDto,
} from '../dto/seatingPlan.dto';
import SeatingPlanModel from '../database/models/seatingPlan.model';
import { ErrorHandler } from '../helpers/handleResponse.helper';

export const create = async (
  seatingPlanToCreate: SeatingPlanToCreateDto
): Promise<SeatingPlanCreatedDto> => {
  try {
    const seatingPlanCreated = await new SeatingPlanModel(
      seatingPlanToCreate
    ).save();
    return { id: seatingPlanCreated.id };
  } catch (error) {
    throw new ErrorHandler(error.message, 400);
  }
};

export const get = async (): Promise<SeatingPlanDto> => {
  try {
    return await SeatingPlanModel.findOne({})
      .sort({ createdAt: -1 })
      .populate('tables', { tableNumber: 1, numberOfSeats: 1 });
  } catch (error) {
    throw new ErrorHandler(error.message, 400);
  }
};
