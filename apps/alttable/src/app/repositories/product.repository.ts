import { ProductToCreateDto, ProductCreatedDto, ProductToUpdateDto } from '../dto/product.dto';
import productModel from '../database/models/product.model';
import { ErrorHandler } from '../helpers/handleResponse.helper';
import HttpStatusCode from '../helpers/httpStatusCode.helper';

export const create = async (productToCreate: ProductToCreateDto): Promise<ProductCreatedDto> => {
    try {
    const productCreated: ProductCreatedDto = await new productModel(productToCreate).save();
    return productCreated;
  } catch (error) {
    throw new ErrorHandler(error.message, HttpStatusCode.BAD_REQUEST);
  }
};

export const getProductByName = async (productName): Promise<ProductCreatedDto> => {
  // create the product
  try {
    const product = productModel.findOne({ name: productName })
    return product;
  } catch (error) {
    throw new ErrorHandler(error.message, HttpStatusCode.BAD_REQUEST);
  }
};

export const updateQuantity = async (productToUpdate: ProductToUpdateDto): Promise<ProductCreatedDto> => {
    try {
      const product = productModel.findOneAndUpdate({ name: productToUpdate.name }, { quantity: productToUpdate.quantity}, { new: true })
      return product
    } catch (error) {
      throw new ErrorHandler(error.message, HttpStatusCode.BAD_REQUEST);
    }
}

export const getAll = async () => {
  try {
    const product = productModel.find()
    return product
  } catch (error) {
    throw new ErrorHandler(error.message, HttpStatusCode.BAD_REQUEST);
  }
}