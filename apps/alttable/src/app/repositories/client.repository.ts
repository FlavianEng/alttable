import { ClientCreatedDto, ClientToCreateDto } from '../dto/client.dto';
import ClientModel from '../database/models/client.model';
import { ErrorHandler } from '../helpers/handleResponse.helper';
import HttpStatusCode from '../helpers/httpStatusCode.helper';

export const create = async (
  clientToCreate: ClientToCreateDto
): Promise<ClientCreatedDto> => {
  try {
    const clientCreated = await new ClientModel(clientToCreate).save();
    return { id: clientCreated.id };
  } catch (error) {
    throw new ErrorHandler(error.message, HttpStatusCode.BAD_REQUEST);
  }
};
