import {
  TableCreatedDto,
  TableDto,
  TableToCreateDto,
  TableToOpenDto,
} from '../dto/table.dto';
import TableModel from '../database/models/table.model';
import { ErrorHandler } from '../helpers/handleResponse.helper';
import HttpStatusCode from '../helpers/httpStatusCode.helper';

export const create = async (
  tableToCreate: TableToCreateDto
): Promise<TableCreatedDto> => {
  try {
    const tableCreated = await new TableModel(tableToCreate).save();
    return { id: tableCreated.id };
  } catch (error) {
    throw new ErrorHandler(error.message, HttpStatusCode.BAD_REQUEST);
  }
};

export const getByTableNumber = async (
  tableNumber: number
): Promise<TableDto> => {
  try {
    return await TableModel.findOne({ tableNumber });
  } catch (error) {
    throw new ErrorHandler(error.message, HttpStatusCode.BAD_REQUEST);
  }
};

export const getAll = async (): Promise<TableCreatedDto[]> => {
  try {
    return await TableModel.find({}, { id: 1 });
  } catch (error) {
    throw new ErrorHandler(error.message, HttpStatusCode.BAD_REQUEST);
  }
};

export const openTable = async (
  tableToOpen: TableToOpenDto
): Promise<TableCreatedDto> => {
  try {
    const { tableNumber, clients } = tableToOpen;

    const table = await TableModel.findOne({ tableNumber });
    table.clients = clients;
    table.tableState = 'started';
    table.available = false;

    return await table.save();
  } catch (error) {
    throw new ErrorHandler(error.message, HttpStatusCode.BAD_REQUEST);
  }
};
