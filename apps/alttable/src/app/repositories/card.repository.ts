import productModel from '../database/models/product.model';
import { ErrorHandler } from '../helpers/handleResponse.helper';
import HttpStatusCode from '../helpers/httpStatusCode.helper';

export const getCard = async () => {
  try {
    const menu = productModel.find({quantity: {$gt : 0}})
    return menu
  } catch (error) {
    throw new ErrorHandler(error.message, HttpStatusCode.BAD_REQUEST);
  }
}