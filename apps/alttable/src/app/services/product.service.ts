import { ProductToCreateDto, ProductCreatedDto, ProductToUpdateDto } from '../dto/product.dto';
import { ErrorHandler } from '../helpers/handleResponse.helper';
import HttpStatusCode from '../helpers/httpStatusCode.helper';
import * as productRepository from '../repositories/product.repository';

export const create = async (productToCreate: ProductToCreateDto): Promise<ProductCreatedDto> => {

  const productExist = await productRepository.getProductByName(productToCreate.name)
  if (productExist) { throw new ErrorHandler(`product name "${productToCreate.name}" already exist`, HttpStatusCode.BAD_REQUEST) }
  
  const productCreated = await productRepository.create(productToCreate);
  return productCreated
};

export const updateQuantity = async (productToUpdate: ProductToUpdateDto): Promise<ProductCreatedDto> => {

  const productExist = await productRepository.getProductByName(productToUpdate.name)
  if (!productExist) { throw new ErrorHandler(`product name "${productToUpdate.name}" doesn't exist`, HttpStatusCode.NOT_FOUND) }
  
  const productCreated = await productRepository.updateQuantity(productToUpdate);
  return productCreated
};

export const getAllProduct = async () => {

  const productList = await productRepository.getAll()
  return productList
};
