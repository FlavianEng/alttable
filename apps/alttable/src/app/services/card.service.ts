import * as cardRepository from '../repositories/card.repository';

export const getCard = async () => {

  const card = await cardRepository.getCard()
  return card
};