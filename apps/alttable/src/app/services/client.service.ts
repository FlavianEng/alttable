import { ClientCreatedDto, ClientToCreateDto } from '../dto/client.dto';
import * as clientRepository from '../repositories/client.repository';

export const create = async (
  clientToCreate: ClientToCreateDto
): Promise<ClientCreatedDto> => {
  return await clientRepository.create(clientToCreate);
};
