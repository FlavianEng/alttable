import { ErrorHandler } from '../helpers/handleResponse.helper';
import HttpStatusCode from '../helpers/httpStatusCode.helper';
import * as serviceRepository from '../repositories/service.repository';
import * as seatingPlanService from '../services/seatingPlan.service';
import { SeatingPlanDto } from './../dto/seatingPlan.dto';
import { ServiceDetailsDto, ServiceReceivedDto } from './../dto/service.dto';

// Create a service
export const create = async (
  serviceToCreate: ServiceReceivedDto
): Promise<ServiceDetailsDto> => {
  const seatingPlan: SeatingPlanDto = await seatingPlanService.get();
  if (!seatingPlan) {
    throw new ErrorHandler(
      'To create a service, a table plan must already be created',
      HttpStatusCode.BAD_REQUEST
    );
  }

  const serviceDetails: ServiceDetailsDto = await serviceRepository.create({
    ...serviceToCreate,
    seatingPlanId: seatingPlan.id,
  });

  return serviceDetails;
};
