import {
  SeatingPlanCreatedDto,
  SeatingPlanDto,
  SeatingPlanToCreateDto,
} from '../dto/seatingPlan.dto';
import * as seatingPlanRepository from '../repositories/seatingPlan.repository';

export const create = async (
  seatingPlanToCreate: SeatingPlanToCreateDto
): Promise<SeatingPlanCreatedDto> => {
  return await seatingPlanRepository.create(seatingPlanToCreate);
};

export const get = async (): Promise<SeatingPlanDto> => {
  return await seatingPlanRepository.get();
};
