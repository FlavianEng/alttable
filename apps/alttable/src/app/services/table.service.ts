import {
  TableCreatedDto,
  TableDto,
  TableToCreateDto,
  TableToOpenDto,
} from '../dto/table.dto';
import * as tableRepository from '../repositories/table.repository';

export const create = async (
  tableToCreate: TableToCreateDto
): Promise<TableCreatedDto> => {
  return await tableRepository.create(tableToCreate);
};

export const getByTableNumber = async (
  tableNumber: number
): Promise<TableDto> => {
  return await tableRepository.getByTableNumber(tableNumber);
};

export const getAll = async (): Promise<string[]> => {
  const tables = await tableRepository.getAll();
  const tableIds = tables.map((table) => table.id);

  return tableIds;
};

export const openTable = async (
  tableToOpen: TableToOpenDto
): Promise<TableCreatedDto> => {
  return await tableRepository.openTable(tableToOpen);
};
