import { ErrorHandler } from './handleResponse.helper';
import HttpStatusCode from './httpStatusCode.helper';

export async function isUniqueField(
  getByFunction,
  param,
  fieldName?: string
): Promise<void> {
  const res = await getByFunction(param);

  if (res) {
    throw new ErrorHandler(
      `🚨 Validator failed : The field  ${fieldName ?? ''} is not unique`,
      HttpStatusCode.BAD_REQUEST
    );
  }
}

export function isNumberStrictlyPositive(
  value: number,
  fieldName?: string
): void {
  if (value < 1) {
    throw new ErrorHandler(
      `🚨 Validator failed : The field  ${
        fieldName ?? ''
      } is not strictly positive`,
      HttpStatusCode.BAD_REQUEST
    );
  }
}

export function hasPositiveLength(item, fieldName?: string): void {
  if (item.length < 1) {
    throw new ErrorHandler(
      `🚨 Validator failed : The field ${
        fieldName ?? ''
      } has a negative length`,
      HttpStatusCode.BAD_REQUEST
    );
  }
}

export function isObjectDefined(obj, itemName: string): void {
  if (Object.keys(obj).length < 1) {
    throw new ErrorHandler(
      `🚨 Validator failed : ${
        itemName ? itemName + ' is undefined' : 'Cannot read undefined'
      }`,
      HttpStatusCode.BAD_REQUEST
    );
  }
}

export function isDefined(item, itemName: string): void {
  if (!item) {
    throw new ErrorHandler(
      `${itemName} is undefined`,
      HttpStatusCode.BAD_REQUEST
    );
  }
}

export function isGreaterOrEqual(
  firstItem,
  secondItem,
  firstItemName: string,
  secondItemName: string
): void {
  if (firstItem <= secondItem) {
    throw new ErrorHandler(
      `${firstItemName} cannot be lower than ${secondItemName}`,
      HttpStatusCode.BAD_REQUEST
    );
  }
}

export function firstParamIsLowerOrEqual(
  firstItem,
  secondItem,
  firstItemName: string,
  secondItemName: string
): void {
  if (firstItem >= secondItem) {
    throw new ErrorHandler(
      `${firstItemName} cannot be greater than ${secondItemName}`,
      HttpStatusCode.BAD_REQUEST
    );
  }
}

export function isDate(item, itemName: string): void {
  item = new Date(item);
  if (isNaN(item)) {
    throw new ErrorHandler(
      `${itemName} is not a valid date`,
      HttpStatusCode.BAD_REQUEST
    );
  }
}

export function isTrue(item, itemName: string): void {
  if (!item) {
    throw new ErrorHandler(
      `${itemName} is not set to true`,
      HttpStatusCode.BAD_REQUEST
    );
  }
}
