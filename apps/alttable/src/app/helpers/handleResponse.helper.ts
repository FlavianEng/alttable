// Create error
export class ErrorHandler extends Error {
  public readonly statusCode;

  constructor(message: string, statusCode: number) {
    super();
    this.message = message;
    this.statusCode = statusCode;
  }
}

// Send error
export const sendError = (error, res) => {
  const { message, statusCode } = error;

  res.status(statusCode).send({
    status: 'error',
    statusCode,
    message,
  });
};

// Send success
export const sendSuccess = (data, statusCode, res) => {
  res.status(statusCode).send({
    status: 'success',
    statusCode,
    data,
  });
};
