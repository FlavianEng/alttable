import { Router } from 'express';
import * as tableController from '../controllers/table.controller';

const router = Router();

router.post('/', tableController.create);
router.get('/', tableController.getAll);
router.get('/number/:tableNumber', tableController.getByTableNumber);
router.put('/', tableController.openTable);

export default router;
