import { Router } from 'express';
import * as cardController from '../controllers/card.controller';

const router = Router();

router.get('/', cardController.getCard);

export default router;
