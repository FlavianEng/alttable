import { Router } from 'express';
import * as serviceController from '../controllers/service.controller';

const router = Router();

router.post('/', serviceController.create);

export default router;
