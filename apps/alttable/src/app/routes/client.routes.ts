import { Router } from 'express';
import * as clientController from '../controllers/client.controller';

const router = Router();

router.post('/', clientController.create);

export default router;
