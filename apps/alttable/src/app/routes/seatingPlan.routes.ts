import { Router } from 'express';
import * as seatingPlanController from '../controllers/seatingPlan.controller';

const router = Router();

router.post('/', seatingPlanController.create);
router.get('/', seatingPlanController.get);

export default router;
