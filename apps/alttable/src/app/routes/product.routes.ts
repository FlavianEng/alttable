import { Router } from 'express';
import * as productController from '../controllers/product.controller';

const router = Router();

router.post('/add', productController.create);
router.get('/', productController.getAll);
router.put('/update/quantity', productController.updateQuantity);

export default router;
