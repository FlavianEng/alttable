export interface ClientToCreateDto {
  tableId: string;
  tableSeatNumber: number;
}

export interface ClientCreatedDto {
  id: string;
}
