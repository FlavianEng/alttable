export interface ProductToCreateDto {
  name: string;
  description: string;
  type: ProductTypeEnum;
  price: number;
  quantity: number;
}

export interface ProductCreatedDto {
  ProductId: string;
}

export interface ProductToUpdateDto {
  name: string;
  quantity: String;
}
const enum ProductTypeEnum {
  "Apéritif", "Entrée", "Plat", "Desset", "Boisson"
}