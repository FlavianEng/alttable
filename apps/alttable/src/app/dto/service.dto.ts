export interface ServiceReceivedDto {
  openingDate: Date;
  closingDate: Date;
}

export interface ServiceToCreateDto {
  openingDate: Date;
  closingDate: Date;
  seatingPlanId: string
}

export interface ServiceDetailsDto {
  id: string;
}
