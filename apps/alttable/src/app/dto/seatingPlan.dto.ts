import { minimalTableDto } from './table.dto';

export interface SeatingPlanToCreateDto {
  tables: string[];
}

export interface SeatingPlanCreatedDto {
  id: string;
}

export interface SeatingPlanDto {
  id: string;
  tables: minimalTableDto[];
  createdAt: Date;
}
