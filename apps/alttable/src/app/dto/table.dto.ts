export interface TableToCreateDto {
  numberOfSeats: number;
  available: boolean;
  tableNumber: number;
  tableState: string;
}

export interface TableCreatedDto {
  id: string;
}

export interface TableDto {
  id: string;
  numberOfSeats: number;
  available: boolean;
  tableNumber: number;
  tableState: string;
}

export interface SimplifiedTableToOpenDto {
  tableNumber: number;
  numberOfClients: number;
}
export interface TableToOpenDto {
  tableNumber: number;
  clients: string[];
}

export interface minimalTableDto {
  tableNumber: number;
  numberOfSeats: number;
}
