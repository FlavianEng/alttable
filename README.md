# ALT’TABLE

This project was generated using [Nx](https://nx.dev).

## Installation

Install the repository by cloning it

```sh
git clone https://gitlab.com/FlavianEng/alttable.git
```

Install the dependencies

```sh
npm install
```

## Usage

This project is using [Trello](https://trello.com/invite/b/jilLEO7s/2aecdfa7a4b08283d5759b4df07dbc28/organisation).

### For Git

#### Branch

For naming the branch according the UserStory : **\<projectKey\>-US-\<UserStoryId\>**

> ALT-US-4

For naming the other branch : **\<projectKey\>-\<ShortDescription\>**

> ALT-Short-description

#### Commit

For naming the commit : **\<gitmoji\> \<message\>**

> 🎉 Init project

## Development Team

- [Flavian ENGEVIN](https://gitlab.com/FlavianEng) - Master1 Expert in Web Development
- [Kenan DUFRENE](https://gitlab.com/Anebris) - Master1 Expert in Web Development
- [Quentin MASBERNAT](https://gitlab.com/quentin.masbernat) - Master1 Expert in Web Development
